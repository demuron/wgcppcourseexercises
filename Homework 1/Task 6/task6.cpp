﻿/*
	Пользователь вводит с клавиатуры время в секундах, прошедшее с начала дня.
	Вывести на экран текущее время в часах, минутах и секундах.
	Посчитать, сколько часов, минут и секунд осталось до полуночи.
*/

//Ваш код здесь
#include <iostream>
#include <Windows.h>
#include <iomanip>


int MIN = 60;
int HOUR = 60 * MIN;
int DAY = 24 * HOUR;


int main()
{
	SetConsoleCP(1251);// установка кодовой страницы win-cp 1251 в поток ввода
	SetConsoleOutputCP(1251); // установка кодовой страницы win-cp 1251 в поток вывода

	int secondsLeft;
	std::cout << "Введи количество секунд прошедших с начала дня\n";
	std::cin >> secondsLeft;

	secondsLeft = secondsLeft % DAY;
	int hoursLeft = secondsLeft / HOUR;
	int hoursTimeRest = secondsLeft % HOUR;
	int minutesLeft = hoursTimeRest / MIN;
	int secondsTimeRest = hoursTimeRest % MIN;

	int secondsTillMidnight = DAY - secondsLeft;
	int hoursLeftMidnight = secondsTillMidnight / HOUR;
	int hoursTimeRestMidnight = secondsTillMidnight % HOUR;
	int minutesLeftMidnight = hoursTimeRestMidnight / MIN;
	int secondsTimeRestMidnight = hoursTimeRestMidnight % MIN;


	std::cout
		<< std::setw(15) << "TIME"
		<< std::setw(15) << "HOURS"
		<< std::setw(15) << "MIN"
		<< std::setw(15) << "SEC"
		<< std::endl;
	std::cout
		<< std::setw(15) << "Время"
		<< std::setw(15) << hoursLeft
		<< std::setw(15) << minutesLeft
		<< std::setw(15) << secondsTimeRest
		<< std::endl;
	std::cout
		<< std::setw(15) << "До полуночи"
		<< std::setw(15) << hoursLeftMidnight
		<< std::setw(15) << minutesLeftMidnight
		<< std::setw(15) << secondsTimeRestMidnight
		<< std::endl;
	return 0;
}