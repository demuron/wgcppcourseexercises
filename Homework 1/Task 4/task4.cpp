﻿#include <iostream>
#include <Windows.h>

/*
Написать программу, которая преобразует введенное пользователем количество
дней в количество полных недель и оставшихся дней. Например, пользователь ввел 17 дней,
программа должна вывести на экран 2 недели и 3 дня.
*/
const int WEEK = 7;


int main()
{
	SetConsoleCP(1251);// установка кодовой страницы win-cp 1251 в поток ввода
	SetConsoleOutputCP(1251); // установка кодовой страницы win-cp 1251 в поток вывода
	int days;
	std::cout << "Введи количество дней\n";
	std::cin >> days;
	int weeks = days / WEEK;
	int restDays = days % WEEK;
	std::cout << "\nКоличество полных недель = " << weeks;
	std::cout << "\nКоличество оставшихся дней = " << restDays << "\n";
	std::cout << "\nХ..й тебе, а не отпуск!" << std::endl;

	return 0;
}