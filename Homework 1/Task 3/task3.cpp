﻿#include <iostream>

/*
Вычислить пройденное расстояние S при прямолинейном равноускоренном движении по формуле.
Обозначения: v – скорость, t – время, а – ускорение.
*/
const double MIN = 60;
const double H = 60 * MIN;
const double KM = 1000;


int main()
{	
	setlocale(LC_CTYPE, "rus");
	double v = 120 * KM / H;
	double t = 15 * MIN;
	double a = 30 * KM / H / H;
	double s = v*t + (a*t*t) / 2;
	std::cout << "За " << t / MIN << " мин тачила на скорости " << v * H / KM << " км/ч и ускорением " << a;
	std::cout << " м/с\nпроедет " << s / KM << " километрa(ов), как раз от малины до шабанов\n\n";
	return 0;
}
