﻿#include <iostream>
#include <Windows.h>
#include <iomanip>

/*
Пользователь вводит с клавиатуры расстояние, расход бензина на 100 км и стоимость трех видов бензина.
Вывести на экран сравнительную таблицу со стоимостью поездки на разных видах бензина.
*/


int main()
{
	SetConsoleCP(1251);// установка кодовой страницы win-cp 1251 в поток ввода
	SetConsoleOutputCP(1251); // установка кодовой страницы win-cp 1251 в поток вывода

	double gasConsumption;
	std::cout << "Введи расход бенза на 100км\n";
	std::cin >> gasConsumption;

	double gas92cost;
	std::cout << "Введи стоимость 92 бензина\n";
	std::cin >> gas92cost;

	double gas95cost;
	std::cout << "Введи стоимость 95 бензина\n";
	std::cin >> gas95cost;

	double gas98cost;
	std::cout << "Введи стоимость 98 бензина\n";
	std::cin >> gas98cost;

	char destination[128];
	std::cout << "Куда ехать?\n";
	std::cin >> destination;

	double distance;
	std::cout << "Далеко ехать?\n";
	std::cin >> distance;

	bool showRoad;
	std::cout << "Дорогу покажешь?\n";
	std::cin >> showRoad;

	if (!showRoad) {
		std::cout << "Эй ты чего дерзишь? Давай досвидания\n";
		return 1;
	}
	double liters = gasConsumption * distance / 100.0;
	double trip92 = gas92cost * liters;
	double trip95 = gas95cost * liters;
	double trip98 = gas98cost * liters;

	
	std::cout
		<< std::setw(15) << "ПУНКТ НАЗНАЧЕНИЯ"
		<< std::setw(15) << "РАССТОЯНИЕ"
		<< std::setw(15) << "БЕНЗИН"
		<< std::setw(25) << "СТОИМОСТЬ ПОЕЗДКИ"
		<< std::endl;
	std::cout
		<< std::setw(15) << destination
		<< std::setw(15) << distance
		<< std::setw(15) << "92"
		<< std::setw(25) << trip92
		<< std::endl;
	std::cout
		<< std::setw(15) << ""
		<< std::setw(15) << ""
		<< std::setw(15) << "95"
		<< std::setw(25) << trip95
		<< std::endl;
	std::cout
		<< std::setw(15) << ""
		<< std::setw(15) << ""
		<< std::setw(15) << "98"
		<< std::setw(25) << trip98
		<< std::endl;
	
	return 0;
}